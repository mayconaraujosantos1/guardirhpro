# Projeto X - Backlog do Caso de Uso

## Visão Geral do Projeto
O projeto X tem como objetivo permitir que os clientes enviem arquivos para o Google Drive de forma segura, recebam um link protegido por senha via e-mail e definam um tempo de expiração para esse link.

## Backlog

### **Épico: Implementar Envio Seguro para o Google Drive**

1. **Configurar Serviço de Upload (Microserviço 1)**
    - [ ] Iniciar um projeto Spring Boot para o serviço de upload.
    - [ ] Definir Protobuf para comunicação gRPC.
    - [ ] Implementar serviço gRPC para receber e processar arquivos.
    - [ ] Integrar com a API do Google Drive para envio.

2. **Configurar Serviço de E-mail (Microserviço 2)**
    - [ ] Iniciar um novo projeto Spring Boot para o serviço de e-mail.
    - [ ] Definir Protobuf para comunicação gRPC.
    - [ ] Implementar serviço gRPC para enviar e-mails com links protegidos.

3. **Integrar Serviços**
    - [ ] Configurar comunicação gRPC entre os serviços de upload e e-mail.
    - [ ] Implementar lógica para gerar links e notificar o cliente.

### **Épico: Implementar Frontend com Angular**

1. **Configurar Projeto Angular**
    - [ ] Configurar um projeto Angular usando Angular CLI.

2. **Criar Componente de Upload**
    - [ ] Desenvolver um componente para permitir o upload de arquivos.

3. **Criar Componente para Escolha e Confirmação de Senha**
    - [ ] Desenvolver um componente para permitir que o cliente escolha e confirme uma senha.

4. **Integrar com Backend**
    - [ ] Utilizar Angular Service para fazer chamadas gRPC aos microserviços.

5. **Adicionar Notificações**
    - [ ] Utilizar bibliotecas de notificações Angular para informar o usuário sobre o status do processo.

6. **Estilizar Componentes**
    - [ ] Utilizar Angular Styling para estilizar os componentes de forma atraente.

### **Próximos Passos**
1. **Aprimorar Segurança**
    - [ ] Implementar validações adicionais para garantir a segurança das senhas.

2. **Melhorar Experiência do Usuário**
    - [ ] Adicionar animações e feedbacks visuais para melhorar a experiência do usuário.

3. **Testes e Depuração**
    - [ ] Realizar testes rigorosos e depurar quaisquer problemas identificados.

4. **Documentação**
    - [ ] Documentar o código e fornecer instruções detalhadas de configuração.

5. **Deploy**
    - [ ] Preparar o projeto para o ambiente de produção e realizar o deploy.

## Contribuindo
Contribuições são bem-vindas! Sinta-se à vontade para abrir issues, enviar pull requests ou entrar em contato com a equipe de desenvolvimento.

## Licença
Este projeto está licenciado sob a [Licença MIT](LICENSE).
